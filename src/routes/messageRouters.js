import express from 'express';
const router = express.Router();
import {
  getMessageByUser,
  getMessageOneToOne,
  sendMessage,
} from '../controllers/messageController.js';

router.post('/send', sendMessage);
router.get('/:id', getMessageByUser);
router.get('/bygroup/:id', getMessageOneToOne);

export default router;
