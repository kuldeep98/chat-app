import express from 'express'
const router = express.Router()
import { loginUser, registerUser, findUser, getAllUsers } from '../controllers/userController.js'

router.post('/register',registerUser)
router.post('/login',loginUser)
router.get('/find',findUser)
router.get('/',getAllUsers)
export default router