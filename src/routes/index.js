import express from 'express';
const router = express.Router();
import userRouter from './userRoutes.js';
import groupRouter from './groupRouters.js';
import messageRouter from './messageRouters.js';

router.use('/users', userRouter);
router.use('/groups', groupRouter);
router.use('/messages', messageRouter);

export default router;
