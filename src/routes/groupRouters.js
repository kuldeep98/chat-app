import express from 'express';
const router = express.Router();
import {
  createGroup,
  getGroup,
  addFriend,
  addUserToGroup,
  getGroupById,
} from '../controllers/groupController.js';


router.get('/',getGroupById)
router.post('/create/:id', createGroup);
router.post('/addfriend/:id', addFriend);
router.get('/:id', getGroup);
router.patch('/addusertogroup/:id', addUserToGroup);

export default router;
