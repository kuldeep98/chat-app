import Group from '../models/groupModel.js';

const getGroupHandle = async (userid) => {
  const groups = await Group.find({ 'users.user': userid });
  return groups;
};

const getGroupByIdHandle = async (groupid) => {
  const group = await Group.findById(groupid);
  return group;
};

export { getGroupHandle,getGroupByIdHandle };
