import mongoose from 'mongoose';
import Cryptr from 'cryptr';
import config from 'config';
const cryptr = new Cryptr(config.get('jwtsecret'));

const messageSchema = new mongoose.Schema(
  {
    message: {
      type: String,
      required: true,
    },
    from: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Users',
    },
    to: [
      {
        user: {
          type: mongoose.Schema.Types.ObjectId,
          required: true,
          ref: 'users',
        },
        delivered: {
          type: Boolean,
          default: false,
        },
        read: {
          type: Boolean,
          default: false,
        },
      },
    ],
    groupId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'groups',
    },
  },
  {
    timestamps: true,
  }
);

messageSchema.methods.getDecryptedMessage = function (message) {
  let msg = cryptr.decrypt(message);
  return msg;
};

messageSchema.pre('save', async function (next) {
  this.message = await cryptr.encrypt(this.message);
  next();
});

const Message = mongoose.model('messages', messageSchema);

export default Message;
