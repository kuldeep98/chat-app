import jwt from 'jsonwebtoken'
import config from 'config'


const generateToken = (id)=>{
    return jwt.sign({id},config.get('jwtsecret'),{
        expiresIn:'1d'
    })
}

export default generateToken