import User from '../models/userModels.js';
import generateToken from '../utils/generateToken.js';
import { loginSchema, registerSchema } from '../validations/userValidator.js';

// @desc    Register a new user
// @route   POST /api/users/register
// @access  Public

const registerUser = async (req, res) => {
  try {
    const { name, phone, password } = req.body;
    const { errors, value } = await registerSchema.validateAsync({
      name,
      phone,
      password,
    });
    if (errors) return res.status(406).json({ errors });
    const findUser = await User.findOne({ phone });
    if (findUser)
      return res.json({ err: 'User Phone Number Already registered!' });
    const user = await User.create({
      name,
      phone,
      password,
    });

    if (user) {
      res.status(201).json({ ...user._doc, token: generateToken(user._id) });
    } else {
      res.status(400);
      throw new Error('Invalid user data');
    }
  } catch (error) {
    res.send({ err: error.message });
  }
};

// @desc    Auth user & get token
// @route   POST /api/users/login
// @access  Public

const loginUser = async (req, res) => {
  try {
    const { phone, password } = req.body;
    const { errors } = await loginSchema.validateAsync({
      phone,
      password,
    });
    if (errors) return res.status(406).json({ errors });
    const user = await User.findOne({ phone });
    if (user && (await user.matchPassword(password)))
      res.status(200).json({
        _id: user._id,
        name: user.name,
        phone: user.phone,
        token: generateToken(user._id),
      });
    else {
      res.status(401);
      throw new Error('Invalid phone or password');
    }
  } catch (error) {
    res.send({ err: error.message });
  }
};

// @desc    Get user by phone number
// @route   Get /api/users/find
// @access  Public

const findUser = async (req, res) => {
  try {
    const { phone } = req.body;
    const user = await User.findOne({ phone });
    if (user) {
      res.status(200).json({
        _id: user._id,
        name: user.name,
        phone: user.phone,
      });
    } else {
      res.status(404);
      throw new Error('User not Found..!');
    }
  } catch (error) {
    res.send({ err: error.message });
  }
};

// @desc    Get All Users
// @route   Get /api/users/find
// @access  Public

const getAllUsers = async (req, res) => {
  try {
    const user = await User.find({}).select('-password');
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404);
      throw new Error('Users not Found..!');
    }
  } catch (error) {
    res.send({ err: error.message });
  }
};

export { registerUser, loginUser, findUser, getAllUsers };
