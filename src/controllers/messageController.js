import { getGroupByIdHandle } from '../dbHandlers/groupHandlers.js';
import Message from '../models/messageModels.js';

const sendMessage = async (req, res) => {
  try {
    const { message, from, groupId } = req.body;
    let group = await getGroupByIdHandle(groupId);
    console.log(group);
    const users = [];
    group.users
      .filter((c) => c.user != from)
      .forEach((x) => {
        users.push({ user: x.user });
      });
    let messages = await Message.create({
      message,
      from,
      to: users,
      groupId,
    });
    res.json(messages);
  } catch (error) {
    res.json({ err: error.message });
  }
};

const getMessageByUser = async (req, res) => {
  try {
    const userId = req.params.id;
    let messages = await Message.find({ from: userId }).populate('from', [
      'name',
      'phone',
    ]);
    const msgs = [];
    messages.forEach((val) => {
      msgs.push({
        ...val._doc,
        message: val.getDecryptedMessage(val.message),
      });
    });
    console.log(msgs);
    res.json(msgs);
  } catch (error) {
    res.json({ err: error.message });
  }
};

const getMessageOneToOne = async (req, res) => {
  try {
    const userId = req.params.id;
    const { groupId } = req.body;
    console.log(userId);
    console.log(groupId);
    let messages = await Message.find({ groupId });
    const msgs = [];
    messages.forEach((val) => {
      msgs.push({
        ...val._doc,
        message: val.getDecryptedMessage(val.message),
      });
    });
    res.json(msgs);
  } catch (error) {
    res.json({ err: error.message });
  }
};

export { sendMessage, getMessageByUser, getMessageOneToOne };
