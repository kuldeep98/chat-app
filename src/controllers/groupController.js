import {
  getGroupByIdHandle,
  getGroupHandle,
} from '../dbHandlers/groupHandlers.js';
import Group from '../models/groupModel.js';

// @desc    Get Groups By ID
// @route   Get /api/groups/:userid
// @access  Public

const getGroupById = async (req, res) => {
  try {
    const { groupId } = req.body;
    const group = await getGroupByIdHandle(groupId);
    res.json(group);
  } catch (error) {
    res.status(500).json({ err: 'Internal Server Error' });
  }
};

// @desc    Get Groups By User
// @route   Get /api/groups/:userid
// @access  Public

const getGroup = async (req, res) => {
  try {
    const userid = req.params.id;
    const groups = await getGroupHandle(userid);
    res.json(groups);
  } catch (error) {
    res.json({ err: error.message });
  }
};

// @desc    create group by user with some other users
// @route   POST /api/groups/create/:userid
// @access  Public

const createGroup = async (req, res) => {
  try {
    const { name, userids } = req.body;
    const adminId = req.params.id;
    const users = [];
    userids.forEach((ele) => {
      users.push({ user: ele, isAdmin: ele === adminId ? true : false });
    });
    let group = await Group.create({
      name: name,
      users: users,
      createdBy: adminId,
    });
    res.json(group);
  } catch (error) {
    res.json({ err: error.message });
  }
};

// @desc    Add One to One Friend
// @route   POST /api/groups/addfriend/:userid
// @access  Public

const addFriend = async (req, res) => {
  try {
    const { userid } = req.body;
    const adminUser = req.params.id;
    if (userid === adminUser)
      return res.json({ warning: "You can't add yourself as friend..!!" });
    const users = [
      {
        user: userid,
      },
      {
        user: adminUser,
      },
    ];
    let group = await Group.create({
      users,
    });
    res.json(group);
  } catch (error) {
    res.json({ err: err.message });
  }
};

// @desc    Add friend to group
// @route   POST /api/groups/addusertogroup/:groupid
// @access  Public

const addUserToGroup = async (req, res) => {
  try {
    const { userids } = req.body;
    const groupId = req.params.id;
    const users = [];
    userids.forEach((ele) => {
      users.push({ user: ele });
    });
    let group = await Group.findOne({ _id: groupId });
    if (!group) return res.json({ err: "Group doesn't exists" });

    group.users.push(...users);
    await group.save();
    res.json(group);
  } catch (error) {
    res.json({ err: err.message });
  }
};

export { createGroup, addFriend, getGroup, addUserToGroup, getGroupById };
