import express, { json } from 'express';
import dotenv from 'dotenv';
import path from 'path';
import connectDB from './config/db.js';
import indexRouter from './src/routes/index.js';

dotenv.config();
const app = express();

// Middleware

app.use(json({limit:'5mb'}))
connectDB()


//Routes
app.use('/api', indexRouter);

//Server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server started on port: ${PORT}`);
});
